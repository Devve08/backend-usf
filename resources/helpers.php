<?php

if(!function_exists('hasRole')){
    function hasRole($rolename)
    {
        $role = \App\Models\Role::where('name', $rolename)->first();

        if($role){
            if(auth('admin')->user()->role_id == $role->id){
                return true;
            }
        }
        return false;
    }
}

if(!function_exists('hasPermission')) {
    function hasPermission($permissionName)
    {
        $permission = \App\Models\Permission::where('name', $permissionName)->first();
        if($permission){
            if(\App\Models\RolePermission::where([
                ['role_id', auth('admin')->user()->role_id],
                ['permission_id', $permission->id],
            ])->count() > 0){
                return true;
            }
        }
        return false;
    }
}
